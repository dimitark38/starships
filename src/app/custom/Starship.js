export default class Starship {

    constructor(name) {

        this.name = name;
        this._consumables = 0;
        this._passengers = 0;
    }

    set consumables(value) {

        let args = value.split(' ');

        let num = Number(args[0]);
        let period = args[1];

        if (period === 'years' || period === 'year') {

            this._consumables = num * 365;

        } else if (period === 'months' || period === 'month') {

            this._consumables = num * 30;

        } else if (period === 'days' || period === 'day') {

            this._consumables = num * 7;

        }
    }

    set passengers(value) {

        if (value.includes(',')) {
            let temp = value.replace(',', '');
            this._passengers = Number(temp);
        } else {
            this._passengers = Number(value);
        }

    }

    get maxDaysInSpace() {
        return this._consumables / this._passengers;
    }
}