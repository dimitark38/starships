import Starship from './Starship.js';

export default class StarWarsUniverse {

    constructor() {
        this.starships = [];
        //this.init()
    }

    async _getStarshipCount() {
        const resp = await fetch('https://swapi.booost.bg/api/starships/');
        const data = await resp.json();
        const countStarships = data.count;
        return countStarships;
    }

    async _createStarships() {

        let allStarships = [];

        for (let i = 1; i < 37; i++) {

            // const resp = await fetch(`https://swapi.booost.bg/api/starships?page=${i}`);
            // const data = await resp.json();
            // const currentResult = data.results;
            // currentResult.map(r => allStarships.push(r));

            try {
                const resp = await fetch(`https://swapi.booost.bg/api/starships/${i}/`);

                if (resp.status === 200) {
                    const data = await resp.json();
                    allStarships.push(data);
                }
            } catch (error) {
                console.log(error);
            }

        }

        return allStarships;
    }

    async _validateData() {

        const ships = await this._createStarships();

        for (const ship of ships) {

            const nameShip = ship.name;
            const consumable = ship.consumables;
            const passengers = ship.passengers;

            if (consumable !== undefined && consumable !== null && consumable !== 'unknown' &&
                passengers !== undefined && passengers !== null && passengers !== 'n/a' && passengers !== '0' &&
                passengers !== 'unknown') {
                let currentShip = new Starship(nameShip);
                currentShip.consumables = consumable;
                currentShip.passengers = passengers;

                this.starships.push(currentShip);
            }


        }


    }

    async init() {
        const count = this._getStarshipCount();
        const dataShip = this._createStarships();
        await this._validateData();

        return dataShip;
    }

    get theBestStarship() {

        this.starships.sort((a, b) => b.maxDaysInSpace - a.maxDaysInSpace);

        return this.starships[0];
    }
}